import os

from setuptools import setup

project = os.environ.get('PIP_PROJECT', f'{os.getcwd()}/..')

setup(
    name='working_dependency',
    version='1.0',
    python_requires='>=3.6.0',
    py_modules=['working_dependency'],
    install_requires=[
        f'cross_dependency @ file://{project}/cross_dependency',
        ],
    platforms='any',
    zip_safe=True,
    )
