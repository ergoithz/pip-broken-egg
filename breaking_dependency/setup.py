import os

from setuptools import setup

project = os.environ.get('PIP_PROJECT', f'{os.getcwd()}/..')

setup(
    name='breaking_dependency',
    version='1.0',
    python_requires='>=3.6.0',
    py_modules=['breaking_dependency'],
    install_requires=[
        f'cross_dependency @ file://{project}/cross_dependency#egg=cross_dependency',
        ],
    platforms='any',
    zip_safe=True,
    )
