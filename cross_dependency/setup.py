from setuptools import setup

setup(
    name='cross_dependency',
    version='1.0',
    python_requires='>=3.6.0',
    py_modules=['cross_dependency'],
    extras_require={
        'extra': [
            'xxhash>=1.2.0',
            ],
        },
    platforms='any',
    zip_safe=True,
    )
