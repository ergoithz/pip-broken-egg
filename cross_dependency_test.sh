#!/bin/sh
python -m venv env
env PIP_PROJECT=$PWD env/bin/pip install -r requirements.broken.txt || echo -e '\n\e[44m>>> Hash breaks setuptools extras (conflict). \e[49m\n'
env PIP_PROJECT=$PWD env/bin/pip install -r requirements.working.txt && echo -e '\n\e[44m>>> Removing the hash makes it work. \e[49m\n'