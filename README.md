# Pip egg broken demo

Pip cannot properly process versions on nested dependencies when the old `#egg=`
component is added (commonly practice on url dependencies), as it is intended
to override both package name and version.

This repository reproduces the issue.

Run:

```sh
sh ./cross_dependency_test.sh
```